<?php
/**
 * Developer's Theme
 *
 * @package     PragmaRed\Dev
 * @since       1.0.2
 * @author      Nathan Shumate
 * @link        https://pragmared.io
 * @license     GNU General Public License 2.0+
 */
namespace PragmaRed\Dev;

include_once( 'lib/init.php' );

include_once( 'lib/functions/autoload.php' );

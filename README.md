# Genesis Developer's Starter Child Theme

This is a developer-friendly starter child theme for the [Genesis theming framework](http://my.studiopress.com/themes/genesis/) which includes a modular architecture, Sass, gulp, Bourbon, Neat, and Composer.  It is your starting child theme for all of your projects to save you time and moola.

## Features

This theme includes the following features:

- Modular programming architecture
- Configuration-based architecture
- Modular CSS via Sass
- Task runner uses gulp
- Uses Bourbon and Neat

## Dependencies

This child theme requires the following dependencies:

- [Genesis framework](http://my.studiopress.com/themes/genesis/)
- [WordPress](https://wordpress.org/download/)
- Node.js and npm installed on your local machine

## Installation

The theme needs for you to install the node modules in order to run gulp.  To do that, navigate into the theme and in the command line (terminal or Bash), type: `npm install` and then enter.  

## Contributors

All feedback, bug reports, and pull requests are welcome.

<?php
/**
 * Sidebar (widgetized areas) HTML markup structure
 *
 * @package     PragmaRed\Dev
 * @since       1.0.0
 * @author      Nathan Shumate
 * @link        https://pragmared.io
 * @license     GNU General Public License 2.0+
 */
namespace PragmaRed\Dev;

/**
 * Unregister sidebar callbacks.
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_sidebar_callbacks() {

}
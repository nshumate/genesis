<?php
/**
 * Header HTML markup structure
 *
 * @package     PragmaRed\Dev
 * @since       1.0.0
 * @author      Nathan Shumate
 * @link        https://pragmared.io
 * @license     GNU General Public License 2.0+
 */
namespace PragmaRed\Dev;

/**
 * Unregister header callbacks.
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_header_callbacks() {

}